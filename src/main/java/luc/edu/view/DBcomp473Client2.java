package luc.edu.view;
import edu.luc.*;
import java.util.*;
import edu.luc.facility.Facility;
import edu.luc.facility.Office;
import edu.luc.facility.Unit;
import edu.luc.persons.Owner;
import edu.luc.persons.Person;
public class DBcomp473Client2 {
	
	
	public class BookStoreClient2 {//this is a test class for a single DB table
		public void main (String args[]) throws Exception {
			
			//Client will use the customer service to have access to anything related to customer functionality.
		      System.out.println("*************** Creating Person service object (Owner sub-type)*************************");
			//CustomerService custService = new CustomerService();  
			Person person=new Owner();
			  ///////////////////////////////////////////////iiii/////////////////////
	
			System.out.println("DBcomp473Client2: *************** instantiating a person and its address *************************");
	      
			person.setFname("Bob");
	        person.setLname("Lee");
	        person.setId("ZZ7788");
	        
	        Unit office = new Office();
	      //office.setId("AD9090");
	        office.setStreet("500 West Michigan Ave.");
	        office.setUnit("Suite 1000");
	        office.setCity("Chicago");
	        office.setState("IL");
	        office.setZip("60601");
	       
	        //save person information
	        //Saving the newly created person and its address
	        Facility f=new Facility();
	        f.addNewFacility(office);
	        
	        System.out.println("BookStoreClient2: *************** Office facility is inserted in facility Database *************************");
	
      
		}
		}
	}




