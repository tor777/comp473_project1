package edu.luc.PersonsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import edu.luc.*;
import edu.luc.persons.Owner;
import edu.luc.persons.Person;
import edu.luc.PersonsDAO.PersonsDAO3;



public class PersonsDAO extends Person {
//	public ArrayList<String>p=new ArrayList<String>();
	
	
	
	public Person getPerson(String person_status) {//owner, inspector, consumer
		   
	    try { 		
	//Get Person
	            //String id ;
	    	   //String fname ;
	    	  //String lname ;
	    	 //String adress ;  skipped for now
	    	//String status ;
	    	
	    	Person pers=new Owner();
	    	
	    	if(person_status.equals("inspector")){
	    	pers.setStatus(person_status);
	    	pers.setFname("Mike");
	    	pers.setLname("Simpson");
	    	pers.setId("10");
	    	
	    	  System.out.println("\n\nPerson status: "+person_status+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
	      	
	    	}else if(person_status.equals("owner")){
	    		pers.setStatus(person_status);
		    	pers.setFname("Bob");
		    	pers.setLname("Anderson");
		    	pers.setId("15");
		    	
		    	  System.out.println("\nPerson status: " + person_status+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
			      	
		    		
	    	}else if(person_status.equals("consumer")){
	    			
	    	pers.setStatus(person_status);
	    	pers.setFname("Barbar");
	    	pers.setLname("Green");
	    	pers.setId("17");
	    	
	    	  System.out.println("\nPerson status: " + person_status+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
		      	
	    		
	    	}else{System.out.println("Error!!!! Please provide the proper person status(owner, consumer, or inspector");
	    	}
	    
	    	Connection con = DBHelper.getConnection();
	    	 PreparedStatement personPst2;
	    	
	    ////Creates the Person tables in your PostgresSQL database    :))   IT WORKS!! JUST UN-comment it out!!!
	     
	    	Statement st1=DBHelper.getConnection().createStatement();
	    	String createTablePerson="CREATE TABLE person(status varchar(32), fname varchar(32), lname varchar(32), id varchar(32));";
	    	ResultSet custRS1 = st1.executeQuery(createTablePerson);
	    	System.out.println("PersonDAO: *************** Query " + createTablePerson);
	 ///////////////////////
	    //	addPerson(person_status);
	    	//Insert the person object into DB table Person
            String personStm = " INSERT INTO person(status, fname, lname, id) VALUES(?,?,?,?)";
            personPst2 = con.prepareStatement(personStm);
            personPst2.setString(1, pers.getStatus());
            personPst2.setString(2, pers.getFname());       
            personPst2.setString(3, pers.getLname());
            personPst2.setString(4, pers.getId());
            personPst2.executeUpdate();
            
         
	    	Statement st = DBHelper.getConnection().createStatement();
	    	//DB table:  Person
	    	String selectCustomerQuery = "SELECT p.fname, p.lname, p.status FROM person p WHERE p.status = '" + person_status + "'"+";";

	    	ResultSet custRS = st.executeQuery(selectCustomerQuery);      
	    	System.out.println("PersonDAO: *************** Query " + selectCustomerQuery);
	    	
	      ////Get Person
    	  Person person=new Owner();
    	  person.setStatus(person_status);
    	  
    	   	  
	         //close to manage resources
	      custRS.close();
	     
	      
	      //Get the Person Info
	      String selectPersonInfoQuery = "SELECT p.status, p.fname, p.lname, p.id FROM person p WHERE status = '" + person_status + "'";
	      ResultSet addRS = st.executeQuery(selectPersonInfoQuery);
    //	  Address address = new Address();
    	  
    	  System.out.println("PersonDAO: *************** Query " + selectPersonInfoQuery);
    	 
    	 
    	 
	      while ( addRS.next() ) {
	    	
	    	  person.setFname(addRS.getString("fname"));
	    	  person.setLname(addRS.getString("lname"));
	    	  person.setStatus(addRS.getString("status"));
	    	  person.setId(addRS.getString("id"));
	    	  
	      }
	      
	     	        // person.getStatus(person_status);
	      //close to manage resources
	      addRS.close();
	      st.close();
	      
	      return person;
	    }	    
	    catch (SQLException sql_e) {
	      System.err.println("PersonDAO: Threw a SQLException retrieving the Person object.");
	      System.err.println(sql_e.getMessage());
	      sql_e.printStackTrace();
	    }
	    
	    return null;
	  }
	
	
	
	public void addPerson(Person p) {   //public void addPerson(String person_status
		Connection con = DBHelper.getConnection();
        PreparedStatement personPst = null;
        PreparedStatement addPst = null;

        try {
        	//Insert the person object
            String personStm = " INSERT INTO person(status, fname, lname, id) VALUES(?,?,?)";
            personPst = con.prepareStatement(personStm);
            personPst.setString(1, p.getId());
            personPst.setString(2, p.getFname());       
            personPst.setString(3, p.getLname());
            personPst.executeUpdate();
            
         
        } catch (SQLException ex) {

        } finally {

            try {
                if (personPst != null) {
                //	addPst.close();
                	personPst.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
      	      System.err.println("CustomerDAO: Threw a SQLException saving the person object.");
    	      System.err.println(ex.getMessage());
            }
        }
    }

}


