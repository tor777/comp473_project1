package edu.luc.facilityMaintainance;

public interface IFacilityMaintainance {
int makeFacilityMaintainaceRequest();
String getMaintainanceInfo();
void scheduleMaintenance(int schedule_maintenance);
String getFacilityName();
}
